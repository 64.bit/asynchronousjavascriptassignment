const fs = require('fs');
const path = require('path');

//comparator to sort in ascending order
function comparator(a, b) {

    if (a < b)
        return -1;

    else if (a > b)
        return 1;

    else
        return b;
}

function showError(err) { // function for error handling

    console.log(`error : ${err}`);

}
function appendFileName(fileName) { //function for appending text to the file

    fs.appendFile(path.resolve(__dirname, 'filenames.txt'), `${fileName} `, (err) => {
        if (err)
            console.log(`error : ${err}`);
    });

}
function deleteFilesSequentially(){ //function for deleting all the file name that exists in file names.js

    fs.readFile(path.resolve(__dirname,'filenames.txt'),(err,data)=>{

        if(err)
            return showError(err);

        const allCreatedFiles=data.toString().split(' '); //converting the data of filenames.txt to array of strings this will help to loop through the filenames

         allCreatedFiles.pop();//this is for extracting last element as last element is an empty string

        for(let index=0;index<allCreatedFiles.length;index++){

            fs.rm(path.resolve(__dirname,allCreatedFiles[index]),(err)=>{//removing file sequentially

                if(err)
                    return showError(err);

            });
        }

        fs.writeFile(path.resolve(__dirname,'filenames.txt'),'',(err)=>{ //deleting every thing from filenames.txt

            if(err)
            return showError(err);
        })

    })
}
function createFileThree(){ //this will create third file after performing the required operations on data of second file

    fs.readFile(path.resolve(__dirname,'fileTwo.txt'),(err,data)=>{

        if(err)
            return showError(err);

        const fileName='fileThree.txt';

        appendFileName(fileName);

        const sortedData = data.toString().replace(/\n/g, ' ').split(' ').sort(comparator); //sorting in non-decreasing order after replacing new line with blank spcces and spliting 

        let sortedDataString = "";//this will hold data from sorted array in string formate

        for (let index = 0; index < sortedData.length; index++)
        sortedDataString += sortedData[index];

        fs.writeFile(path.resolve(__dirname,fileName),sortedDataString,(err)=>{ //creating 3rd file and writing the data together

            if(err)
                return showError(err);

            deleteFilesSequentially();
        })
    })
}
function createFileTwo(){ //this will create second file after manipulating data of first file
   fs.readFile(path.resolve(__dirname,'fileOne.txt'),(err,data)=>{

       if(err)
        return showError(err);

       const fileName='fileTwo.txt';

       appendFileName(fileName);

       const replacedData = data.toString().toLocaleLowerCase().replace(/\./g, '\n');  

       fs.writeFile(path.resolve(__dirname,fileName),replacedData,(err)=>{ //writing data to second file

           if(err)
           return showError(err);

           createFileThree();
       });
   });
}
function createFileOne(data) { //for creating first file and after manipulating data of lipsum.txt

    const fileName = 'fileOne.txt';

    appendFileName(fileName);

    const capitalData = data.toString().toLocaleUpperCase();

    fs.writeFile(path.resolve(__dirname, fileName), capitalData, (err) => { //writing data of lipsum file to new file after converting it to capital letters

        if (err)
            return showError(err);

        createFileTwo();
    })
}
function createFileAsynchronously() {

    fs.readFile(path.resolve(__dirname, 'lipsum.txt'), (err, data) => { //reading the data from lipsum.txt

        if (err)
            return showError(err);

        createFileOne(data);
    })
}
module.exports=createFileAsynchronously;