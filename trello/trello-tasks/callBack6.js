const callBack2 = require('./callBack2');
const callBack3 = require('./callBack3');

const boards = require('../trello-callbacks/boards.json');


function callBack5() {

    const thanosBoard = boards.find((board) => board.name === 'Thanos');//getting thanos board

    const thanosBoardId = thanosBoard.id;//extracting thanos board id from thanos board

    callBack2(thanosBoardId, (res) => {//for getting lists associated with thanos board id

        const thanosLists = res;
        
        console.log(thanosBoard,thanosLists);

        for(let index=0;index<thanosLists.length;index++){

            callBack3(thanosLists[index].id,(res)=>{//for getting cards associated with current list

                console.log(res);

            })
        }
    });
}

module.exports=callBack5;