const callBack2=require('../callBack2');
const testCallBack2=()=>{

    //valid parameter passed
    callBack2('mcu453ed',(res)=>{

        console.log(res);

    });
    
    //Invalid parameter passed
    callBack2('dsabjd',(res)=>{

        console.log(res);

    });

    callBack2(undefined,(res)=>{

        console.log(res);
        
    });
}
testCallBack2();