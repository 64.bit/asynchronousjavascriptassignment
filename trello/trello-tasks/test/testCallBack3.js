const callBack3=require('../callBack3');

const testCallBack3=()=>{

    //valid parameter passed
    callBack3('qwsa221',(res)=>{

        console.log(res);

    });

    //invalid parameter passed
    callBack3('faslafd',(res)=>{

        console.log(res);

    });
    callBack3(undefined,(res)=>{

        console.log(res);

    });
}
testCallBack3();